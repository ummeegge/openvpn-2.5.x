# openvpn-2.5.x

Transition from IPFires OpenVPN-2.5.x to 2.6.0. Started with IPFire Core 162.

A Wiki which explains what the patches are doing and how to use it is located in here --> https://gitlab.com/ummeegge/openvpn-2.5.x/-/wikis/Explanation-of-the-OpenVPN-transition-from-2.5.x-to-2.6.x-patches .

All commits are located here --> https://gitlab.com/ummeegge/openvpn-2.5.x/-/commits/main .

